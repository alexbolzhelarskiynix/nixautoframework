package nix.training.autointensive.page

import geb.Page

class ProductsPage extends Page {

    static content = {
        products (wait: true){ $(".post>div")}
    }

    static at = {
        title == "NIX Products for Mobile & Desktop | NIX"
    }

    def "User verifies products contain a word"(expectedWord){
        assert products*.text()*.toLowerCase().every { it.contains(expectedWord) }
        products*.text().each{ assert it.contains(expectedWord) }
        return true
    }
}
