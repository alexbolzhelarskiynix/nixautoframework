package nix.training.autointensive.spec

import geb.spock.GebReportingSpec
import nix.training.autointensive.page.LandingPage
import nix.training.autointensive.page.ProductsPage

class NixCorporateSiteSpec extends GebReportingSpec {

    def "Navigate to main page"(){
        when:
            to LandingPage

        and:
            "User navigates to Product page"()

        then:
            at ProductsPage
            "User verifies products contain a word"("iOS")
            "User verifies products contain a word"("nix")
            "User verifies products contain a word"("ANDROID")
    }
}